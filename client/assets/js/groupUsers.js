(function(){
  angular.module('groupUserManager', [])
    .directive("groupUserViewer", ['groupService', 'groupConfigService', 'userService'
        ,function(groupService, groupConfigService, userService) {
      return {
        restrict: 'E',
        templateUrl: "templates/group-users.html",
        controller: function() {
            //this.total = 1000;
            //this.currentPage = 1;
            var self = this;

            this.newGroupName = '';
            this.currentGroup = {};
            this.groups = groupService.query();
            this.members = [];
            /*this.users = [
              {id:11, name:"user1"}
              */
            this.users = userService.query();

            this.addMember = function(user){
              console.log(user);
              if(!this.currentGroup.members){
                this.currentGroup.members = [];
              }
              if(this.currentGroup.members.includes(user)){
                console.log('already in this group');
              }else{
                this.currentGroup.members.push(user);
              }
            };

            this.selectGroup = function(group){
              this.currentGroup = group;

              for(var j = 0; j< this.currentGroup.users.length; j++){
                var userId = this.currentGroup.users[j];
                for(var i = 0; i < this.users.length; i++){
                  var user = this.users[i];
                  if(userId == user.userId){
                    this.addMember(user);
                    break;
                  }
                }
              }
            };

            this.saveMembersInGroup = function(){
                var userIds = [];
                this.currentGroup.members.forEach(function(element){
                  userIds.push(element.userId);
                });

                groupConfigService.addMember({groupId:this.currentGroup.id}
                  ,userIds
                  ,function(value){
                    console.log('success saving group members');
                    alert('Successfully saved in Group '+self.currentGroup.name+'['+value[0]+'] users.');
                  },
                  function(httpResponse){
                    console.log('Problems when saving group members');
                    alert('Problems when saving group members');
                  }
                );

            };

            this.addNewGroup = function(){
              var body = {
                name: this.newGroupName,
                description: this.newGroupName
              };

              groupService.save({},body,
                function(value){
                  console.log('success saving group');
                  self.groups.push(value);
                  self.newGroupName = '';
                },
                function(httpResponse){
                  console.log('failed group save');
                }
              );
            };
        },
        controllerAs: "uCtrl"
      };
    }]);  
})();