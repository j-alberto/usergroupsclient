(function(){
  var ibdbAccessUrl = 'http://localhost:8080/siuapi/';

  angular.module('ibdbServices', ['ngResource']).
	factory('groupService', ['$resource', function($resource){
  		return $resource(ibdbAccessUrl + 'authority/group',{}, {
  			query: {method:'GET', params: {}, isArray:true},
  			save: {method:'POST'}
  		});
  	}]).
  factory('groupConfigService', ['$resource', function($resource){
      return $resource(ibdbAccessUrl + 'authority/group/:groupId/:elementName',{}, {
        addStudy: {method:'POST', params: {elementName: 'study'}, transformResponse: []},
        addMember: {method:'POST', params: {elementName: 'member'}, transformResponse: []}
      });
  }]).
  factory('userService', ['$resource', function($resource){
  	return $resource(ibdbAccessUrl + 'authority/user/:userId',{}, {
  		query: {method:'GET', isArray:true}
  	});
  }]).
  factory('studyService', ['$resource', function($resource){
    return $resource(ibdbAccessUrl + 'study',{}, {
      query: {method:'GET'}
    });
  }])
})();