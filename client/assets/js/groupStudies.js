(function(){
  angular.module('groupStudyManager', [])
    .directive("groupStudyViewer", ['groupService','groupConfigService','studyService'
      ,function(groupService,groupConfigService,studyService) {
      return {
        restrict: 'E',
        templateUrl: "templates/group-studies.html",
        controller: function() {
          var self = this;
          this.currentGroup = {};
          this.searchName = '';
          this.groups = groupService.query();
          /*[
            {id:1, name:"group A"},
            {id:1, name:"group B"}
          ];*/
          this.members = [];
          /*this.studies = [
            {id:9911, name:"study 1"},
            {id:9922, name:"study 2"},
            {id:9933, name:"study 3"}
          ];*/
          this.searchStudy = function(){
            console.log(self.searchName);
            studyService.query({name:self.searchName},function(response){
              self.studies = response.content;
            },function(httpResponse){
              console.log('Problems when retrieving group-studies');
              alert('Problems when retrieving group-studies');
            });
          }

          this.searchStudy();

          this.addMember = function(study){
            console.log(study);
            if(!this.currentGroup.members){
              this.currentGroup.members = [];
            }
            this.currentGroup.members.push(study);
          };

          this.selectGroup = function(group){
            this.currentGroup = group;
          };

          this.saveStudiesInGroup = function(){
            var studyIds = {};
            this.currentGroup.members.forEach(function(study){
              studyIds[study.id]=study.program;
            });

            groupConfigService.addStudy({groupId:this.currentGroup.id}
              ,studyIds
              ,function(value){
                console.log('success saving group studies');
                alert('Successfully saved in Group '+self.currentGroup.name+'['+value[0]+'] studies.');
              },
              function(httpResponse){
                console.log('Problems when saving group studies');
                alert('Problems when saving group studies');
              }
            );
          };
        },
        controllerAs: "sCtrl"
      };
    }]);  
})();